﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class PlayerNavigation : MonoBehaviour
{
    private LineRenderer line;

    private NavMeshAgent navAgent;

    public GameObject cube;

    private NavMeshPath path;

    void Awake()
    {
        line = GetComponent<LineRenderer>();
        navAgent = GetComponent<NavMeshAgent>();
        path = navAgent.path;
    }

    void Start()
    {
        navAgent.updateRotation = true;
    }


    void Update()
    {
        
         line.SetPosition(0, transform.position);
     
        
        
        NavMesh.CalculatePath(navAgent.transform.position, cube.transform.position, NavMesh.AllAreas, path); //Saves the path in the path variable.

        navAgent.SetPath(path);
        navAgent.SetDestination(cube.transform.position);   

        Vector3[] corners = path.corners;
        line.SetPositions(corners);

        if(!navAgent.isStopped)
        {
            var targetPosition = navAgent.pathEndPosition;
            Debug.Log("Path End Position = " + targetPosition);

            var targetPoint = new Vector3(targetPosition.x, transform.position.y, targetPosition.z);
            Debug.Log("target Point = " + targetPoint);

            var _direction = (targetPoint - transform.position).normalized;
            Debug.Log("Direction = " + _direction);

            var _lookRotation = Quaternion.LookRotation(_direction);
            Debug.Log("Rotation = " + _lookRotation);


        }
        
        //yield WaitForEndOfFrame();

        //DrawPath(navAgent.path);


    }
}
